var WRITE_CMD_CMD = ['python', '/Users/syzhou/10gen/write-perf/write_test.py', 'localhost:30999'];

var options = {

    mongosOptions : { binVersion : "" },
    configOptions : { binVersion : "" },
    shardOptions : { binVersion : "" },

    separateConfig : true,
    sync : false
};

var st = new ShardingTest({ shards: 1, mongos: 1, other: options });
st.adminCommand({ enablesharding : "test" });
var db = st.getDB("test");
db.getMongo().forceWriteMode("commands");
var res = db.adminCommand({ setParameter: 1, useClusterWriteCommands: true });

run.apply(null, WRITE_CMD_CMD);
// while (true) sleep(1)
st.stop();