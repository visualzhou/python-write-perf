#!/usr/bin/env python

import pymongo
import sys
import time
import math

class TestCase(object):
  def __init__(self, coll):
    self.coll = coll

  def run(self):
    pass

  def name(self):
    return type(self).__name__

  def setup(self):
    self.coll.drop()
    self.coll.insert({"_id": 0})

  def cleanup(self):
    self.coll.drop()


class InsertEmpty(TestCase):
  '''insert 1000 empty docs'''

  TIMES = 1000

  def __init__(self, coll):
    super(InsertEmpty, self).__init__(coll)

  def run(self):
    for i in xrange(InsertEmpty.TIMES):
      self.coll.insert({})

  def qps(self, duration):
    return InsertEmpty.TIMES / duration


class Insert1K(TestCase):
  '''insert 1000 1k docs'''
  TIMES = 1000

  def __init__(self, coll):
    super(Insert1K, self).__init__(coll)

  def run(self):
    str = "a" * 1024
    doc = {"x": str}
    for i in xrange(Insert1K.TIMES):
      if "_id" in doc:
        del doc["_id"]
      self.coll.insert(doc)

  def qps(self, duration):
    return Insert1K.TIMES / duration

class Insert1M(TestCase):
  '''insert 10 1MB docs'''

  TIMES = 10

  def __init__(self, coll):
    super(Insert1M, self).__init__(coll)

  def run(self):
    str = "a" * 1024 * 1024 # 10M
    doc = {"x": str}
    for i in xrange(Insert1M.TIMES):
      if "_id" in doc:
        del doc["_id"]
      self.coll.insert(doc)

  def qps(self, duration):
    return Insert1M.TIMES / duration

class Update1Doc(TestCase):
  '''insert 1 doc, timer start, update single { $inc: 1 } 1000 times, timer stop'''

  TIMES = 1000

  def __init__(self, coll):
    super(Update1Doc, self).__init__(coll)

  def run(self):
    doc = { "_id": 0 }
    update = { "$inc": { "x": 1 } }
    for i in xrange(Update1Doc.TIMES):
      self.coll.update(doc, update)

  def qps(self, duration):
    return Update1Doc.TIMES / duration



class UpdateAllDoc(TestCase):
  '''insert 1000 docs, timer start, update all { $inc: 1 } 100 times, timer stop'''

  TIMES = 100

  def __init__(self, coll):
    super(UpdateAllDoc, self).__init__(coll)

  def setup(self):
    '''insert 1000 docs'''

    self.coll.drop()
    for i in xrange(1000):
      self.coll.insert({"_id": i})

  def run(self):
    doc = { }
    update = { "$inc": { "x": 1 } }
    for i in xrange(UpdateAllDoc.TIMES):
      self.coll.update(doc, update, multi=True)

  def qps(self, duration):
    return UpdateAllDoc.TIMES / duration


class RemoveOne(TestCase):
  '''insert 1000 docs, timer start, remove one by one, timer stop'''

  TIMES = 1000

  def __init__(self, coll):
    super(RemoveOne, self).__init__(coll)

  def setup(self):
    '''insert 1000 docs'''

    self.coll.drop()
    for i in xrange(RemoveOne.TIMES):
      self.coll.insert({"_id": i})

  def run(self):
    for i in xrange(RemoveOne.TIMES):
      self.coll.remove({ "_id": i })

  def qps(self, duration):
    return RemoveOne.TIMES / duration

class TestRunner(object):
  def stat(self, array):
    mean = sum(array) / len(array)
    std = math.sqrt(sum((x-mean) ** 2 for x in array) / len(array))
    return mean, std


  def perf_test(self, test_case):
    durations = []
    for i in xrange(100):
      test_case.setup()
      start = time.time()
      test_case.run()
      end = time.time()
      test_case.cleanup()
      durations.append(end - start)

    mean, std = self.stat(durations)
    print "WRITE_CMD", test_case.name(), "duration:", mean, "std:", std, "qps:", test_case.qps(mean)


def run_test(test_case):
  test_case.setup()
  start = time.time()
  test_case.run()
  end = time.time()
  test_case.cleanup()

  print "WRITE_CMD", test_case.name(), "duration:", end - start, "qps:", test_case.qps(end - start)

def main():
  url = sys.argv[1]
  client = pymongo.MongoClient(url)
  coll = client.test.foo

  runner = TestRunner()

  for test_case in [InsertEmpty(coll), Insert1K(coll), Insert1M(coll),
      Update1Doc(coll), UpdateAllDoc(coll), RemoveOne(coll)]:

    runner.perf_test(test_case)


if __name__ == "__main__":
  main()
